package com.example.marsphotos.data.apiservice

import retrofit2.Retrofit
import retrofit2.http.GET

import kotlinx.serialization.json.Json
import okhttp3.MediaType
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory

private const val BASE_URL =
    "https://android-kotlin-fun-mars-server.appspot.com"


private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()

object MarsApi{
    val retrofitService : MarsApiService by lazy {
        retrofit.create(MarsApiService::class.java)
    }
}


interface MarsApiService {
    //aqui van las funciones que llaman a la API

    @GET("photos") //La parte variable de la url base
    suspend fun getPhotos() :  List<MarsDto>
}

