package com.example.marsphotos.ui.model.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class MarsDto(
    @SerialName ("id") val id: String,
    @SerialName( "img_src")  val imgSrc: String

)


