package com.example.marsphotos.ui.model.model.mapper


import com.example.marsphotos.model.MarsPhoto
import com.example.marsphotos.ui.model.model.MarsDto
import com.example.marsphotos.ui.model.model.MarsUIModel

class MarsDtoUIModelMapper {

    fun map(listDto: List<MarsPhoto>) : List<MarsUIModel> {
        return mutableListOf<MarsUIModel>().apply {
            listDto.forEach {
                add(MarsUIModel(url = it.imgSrc, id = it.id))
            }
        }
    }
}